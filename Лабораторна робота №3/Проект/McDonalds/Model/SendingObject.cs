﻿using System;
using System.Collections.Generic;

namespace Model
{
    [Serializable]
    public class SendingObject
    {
        public User user { get; set; }
        public List<Model.OrderedFood> orderedFood { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public Status status { get; set; }
        public int mainDishId { get; set; }
    }
}