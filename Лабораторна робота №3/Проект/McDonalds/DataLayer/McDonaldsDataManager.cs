﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
    public class McDonaldsDataManager
    {
        public List<Model.MainDish> SelectMainDishes()
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    return context.MainDishes.Select(
                        d => new Model.MainDish()
                        {
                            Id = d.Id,
                            name = d.name
                        }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new List<Model.MainDish>();
            }
        }

        public List<Model.CategoryDish> SelectCategoryDishes(int mainDishId)
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    return context.CategoryDishes
                        .Where(cd => cd.main_dish_id == mainDishId)
                        .Select(
                        c => new Model.CategoryDish()
                        {
                            Id = c.Id,
                            name = c.name,
                            main_dish_id = c.main_dish_id,
                            number = c.number,
                            price = c.price
                        }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new List<Model.CategoryDish>();
            }
        }

        public List<Model.Order> SelectOrders()
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    return context.Orders.Select(
                        o => new Model.Order()
                        {
                            Id = o.Id,
                            sum_price = o.sum_price,
                            order_date = o.order_date,
                            eat_in = o.eat_in
                        }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new List<Model.Order>();
            }
        }

        public List<Model.OrderedFood> SelectOrderedFood()
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    return context.OrderedFoods.Select(
                        of => new Model.OrderedFood()
                        {
                            Id = of.Id,
                            order_id = of.order_id,
                            category_id = of.category_id,
                            number = of.number
                        }).ToList();

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new List<Model.OrderedFood>();
            }
        }

        public Model.User SelectUserByEmailAndPassword(string email, string password)
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    return context.Users.Where(
                        u => u.email.Equals(email) && u.password.Equals(password)
                             ).Select(u => new Model.User()
                    {
                        Id = u.Id,
                        first_name = u.first_name,
                        last_name = u.last_name,
                        email = u.email,
                        password = u.password
                    }).First();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public void RemoveFoodFromCategoryDishes(int categoryDishId, int number)
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    var categoryDish = context.CategoryDishes.FirstOrDefault(d => d.Id == categoryDishId);
                    if (categoryDish != null)
                    {
                        categoryDish.number -= number;
                        context.CategoryDishes.Attach(categoryDish);
                        context.Entry(categoryDish).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void CreateOrder(decimal sumPrice, bool eatIn)
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    var order = new Order();
                    order.sum_price = sumPrice;
                    order.order_date = DateTime.Now;
                    order.eat_in = eatIn;

                    context.Orders.Add(order);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void CreateOrderedFood(List<Model.OrderedFood> orderedFood)
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    var dbOrderedFood = new List<OrderedFood>();
                    orderedFood.ForEach(food =>
                    {
                        OrderedFood tempOrderedFood = new OrderedFood()
                        {
                            order_id = food.order_id,
                            category_id = food.category_id,
                            number = food.number
                        };
                        dbOrderedFood.Add(tempOrderedFood);
                    });

                    context.OrderedFoods.AddRange(dbOrderedFood);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void CreateUser(Model.User user)
        {
            try
            {
                using (var context = new McDonaldsDbEntities())
                {
                    var newUser = new User()
                    {
                        Id = 2,
                        first_name = user.first_name,
                        last_name = user.last_name,
                        email = user.email,
                        password = user.password
                    };
                    context.Users.Add(newUser);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                    Console.WriteLine(e);
                throw;
            }
        }
    }
}
